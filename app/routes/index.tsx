import { useSubmit, useFetcher } from "remix";

import { Input } from "~/components/ui/Input";
import { Button } from "~/components/ui/Button";
import { FormEvent, useState } from "react";

export default function Index() {
  const [user, setUser] = useState<string>("");
  const fetcher = useFetcher();

  const onHandleSubmit = (event: FormEvent) => {
    if (!user || user.length < 3) {
      event.preventDefault();
    }
  };

  const items =
    fetcher?.data?.items?.item?.length > 0 && fetcher.data.items.item;

  return (
    <>
      <fetcher.Form
        className="flex mt-5"
        action="/user/getUser"
        onSubmit={onHandleSubmit}
      >
        <Input
          type="text"
          name="username"
          label="What's your BGG username?"
          className="text-gray-800 w-80"
          onChange={(event) => setUser(event.target.value)}
        />

        <Button className="ml-5" type="submit">
          Search
        </Button>
      </fetcher.Form>

      {items && (
        <ul className="grid grid-cols-3 gap-4">
          {items?.map((item) => {
            const name = item.name[0]._;

            return <li key={name}>{name}</li>;
          })}
        </ul>
      )}
    </>
  );
}

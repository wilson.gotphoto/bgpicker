import type { LoaderFunction } from "remix";

import { bggSdk } from "~/api/sdk";

export const loader: LoaderFunction = async ({ request }) => {
  const url = new URL(request.url);
  const user = url.searchParams.get("username");

  return await bggSdk(`collection?username=${user}`)
    .then((result) => result)
    .catch((err) => err);
};

import urlJoin from "url-join";
import xml2js from "xml2js";

// API URL
const bggApiUrl = "https://www.boardgamegeek.com/xmlapi2/";

const parser = new xml2js.Parser({
  trim: true,
});

// Client that fetches from the API
const client = async function (endpoint: string) {
  const url = urlJoin(bggApiUrl, endpoint);

  return await fetch(url)
    .then((response) => response.text())
    .then((result) => {
      return parser
        .parseStringPromise(result)
        .then((result) => result)
        .catch((err) => err);
    });
};

export const bggSdk = (endpoint: string) => client(endpoint);

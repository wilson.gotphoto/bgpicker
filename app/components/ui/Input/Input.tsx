import clsx from "clsx";
import { InputHTMLAttributes } from "react";

type InputProps = InputHTMLAttributes<HTMLInputElement> & {
  label: string;
  name: string;
};

export const buttonStyles =
  "inline-block px-6 py-2.5 bg-blue-600 uppercase rounded hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:outline-none focus:ring-0 active:bg-blue-800 transition duration-150 ease-in-out";

export const Input = ({
  className,
  name,
  label,
  type,
  ...rest
}: InputProps) => {
  return (
    <>
      <label htmlFor={name} className="sr-only">
        {label}
      </label>
      <input
        id={name}
        name={name}
        placeholder={label}
        className={clsx(
          "block rounded w-auto px-4 py-1",
          className,
          type === "submit" && buttonStyles
        )}
        {...rest}
      />
    </>
  );
};

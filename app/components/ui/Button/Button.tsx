import clsx from "clsx";
import { HTMLInputTypeAttribute, MouseEventHandler, ReactNode } from "react";

export interface ButtonProps {
  isLoading?: boolean;
  disabled?: boolean;
  children: ReactNode;
  onClick?: MouseEventHandler<HTMLButtonElement>;
  className?: string;
  type?: "submit" | "button";
}

const buttonClassName =
  "active:bg-blue-800 bg-blue-600 duration-150 ease-in-out focus:bg-blue-700 focus:outline-none focus:ring-4 font-medium hover:bg-blue-700 hover:shadow-lg inline-flex items-center px-5 py-2.5 rounded text-white transition transition-colors uppercase";

export const Button = ({
  isLoading = false,
  disabled,
  children,
  onClick,
  className,
  type = "button",
}: ButtonProps) => {
  return (
    <button
      type={type}
      className={clsx(
        { "text-opacity-70": isLoading },
        buttonClassName,
        className
      )}
      disabled={isLoading || disabled}
      onClick={onClick}
    >
      {children}
    </button>
  );
};

export default Button;

import { FC } from "react";

export const Layout: FC = ({ children }) => {
  return (
    <>
      <header>
        <div className="container h-20 flex items-center">
          <h1 className="text-4xl font-bold">Board Game Picker</h1>
        </div>
      </header>
      <main>
        <div className="container">{children}</div>
      </main>
    </>
  );
};

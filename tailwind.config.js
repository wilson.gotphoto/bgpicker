module.exports = {
  content: ["./app/**/*.{ts,tsx,jsx,js}"],
  theme: {
    container: {
      center: true,
      padding: "1rem",
    },
    extend: {},
  },
  variants: {},
  plugins: [require("@tailwindcss/forms")],
};
